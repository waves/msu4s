# The Michigan Four Seasons (MSU-4S) Dataset Tools

![](doc/grid_graphic.jpg)

[Read more about the dataset here](https://egr.msu.edu/waves/msu4s).

# Downloading the Dataset

We recommend using the download scripts provided in this repository. We have
provided a bash script for Linux/Mac users, and a Powershell script for Windows
users.

Downloading the dataset requires approximately 600GB of disk space to download
the compressed dataset, and approximately 2TB to download the dataset plus
uncompressed pre-dedistorted imagery.
