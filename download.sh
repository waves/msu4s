#!/bin/bash

# Michigan State University Four Seasons (MSU-4S) Dataset Download Tool.
# Requirements for using this script:
#  - Compatible UNIX shell (tested on bash, csh, zsh)
#    Windows users: consider using the .ps1 version instead
#  - wget or curl installed and in your PATH.
#  - Enough disk space for the entire dataset. Full download takes
#    approximately 1 TB of space, 2.5TB if you also download the
#    pre-dedistorted images.
#    

# By using or modifying this script to download the MSU Four Seasons
# (MSU-4S) Dataset, you hereby agree to the terms listed below.

__license="The Michigan State University Four Seasons (MSU-4S) Dataset
(c)2024 Michigan State University.

The Michigan State University Four Seasons (MSU-4S) Dataset is provided as-is,
with no warranty or guarantee of fitness for any use, under the Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International license. 

This license requires that reusers give credit to the creator. It allows reusers
to distribute, remix, adapt, and build upon the material in any medium or format,
for noncommercial purposes only. If others modify or adapt the material, they
must license the modified material under identical terms."

# Which tool to use when downloading. Defaults to wget if none selected.
# Script has been tested with wget and curl; use others at your own risk.
DOWNLOAD_TOOL=""
# Download rate limit. Supports either bytes per second, or byte prefixes.
# Leaving this blank will disable local download rate limits.
# Example: 1000, 1k, 2M
DOWNLOAD_LIMIT="10"
# To choose segments to download, add/remove them from the following line
# Valid segments: 2022_rain 2022_spring 2022_spring_snow 2023_early_fall 2023_fall_sunset 2023_fall_sunset_2 2023_late_summer 2023_neighborhood_fall 2023_snow
SEGMENTS=("2022_rain" "2022_spring" "2022_spring_snow" "2023_early_fall" "2023_fall_sunset" "2023_fall_sunset_2" "2023_late_summer" "2023_neighborhood_fall" "2023_snow")
# To choose data channels, add/remove them from the following line
# Valid sources: misc oust top_left top_left_dd top_left_dd_jpg top_mid top_mid_dd top_mid_dd_jpg top_right top_right_dd top_right_dd_jpg
DATA_SOURCES=("misc" "top_left" "top_mid" "top_right" "oust" "velo")
# Different versions of the labels are available
# To change download directory, specify here
OUTPUT_DIR=""
# Change this to 1 to automatically accept the above license and skip the check.
ACCEPT_LICENSE=

function exit_failure () {
    if [ ! -z "$1" ]; then
        echo $1
    else
        echo "Unspecified error occurred."
    fi
    exit 1
}

function check_dl_tool () {

if [[ -z "$DOWNLOAD_TOOL" ]]; then
    which curl > /dev/null
    if [ $? -eq 0 ]; then
        DOWNLOAD_TOOL="curl"
    fi
    which wget > /dev/null
    if [ $? -eq 0 ]; then
        DOWNLOAD_TOOL="wget"
    fi
else
    which $DOWNLOAD_TOOL > /dev/null
    if [ $? -ne 0 ]; then
        exit_failure "Download tool \"$DOWNLOAD_TOOL\" not found."
    fi
fi
}

function dl_options_curl () {
    if [[ ! -z "$DOWNLOAD_LIMIT" ]]; then
        options="$options --limit-rate=$DOWNLOAD_LIMIT"
    fi
    if [[ ! -z "$OUTPUT_DIR" ]]; then
        options="$options --output-dir $OUTPUT_DIR"
    fi
}

function dl_options_wget() {
    if [[ ! -z "$DOWNLOAD_LIMIT" ]]; then
        options="$options --limit-rate=$DOWNLOAD_LIMIT"
    fi
    if [[ ! -z "$OUTPUT_DIR" ]]; then
        options="$options -P $OUTPUT_DIR"
    fi
}

function dl_options () {
    case $DOWNLOAD_TOOL in
    curl)
        dl_options_curl
        ;;
    wget)
        dl_options_wget
        ;;
    esac
}

check_dl_tool
dl_options

echo "options: $options"

read -e -p "Do you agree to these terms? [yN]: " __license_accept 

if [[ ! $__license_accept =~ [Yy]([eE][sS])?$ ]]; then
    exit_failure "License not accepted. Download stopped."
fi

if [ ! -z "$OUTPUT_DIR" ]; then
    mkdir -p $OUTPUT_DIR || exit_failure "Unable to create output directory $OUTPUT_DIR"
fi

for seg in ${SEGMENTS[*]}; do
    for ds in ${DATA_SOURCES[*]}; do
        $DOWNLOAD_TOOL $options mirrors.egr.msu.edu/canvas/msu4s/${seg}/${ds}.tar.gz
    done
done

