# Michigan State University Four Seasons (MSU-4S)
# Dataset Download Tool.
# Requirements for using this script:
#  - Windows Powershell
#    Mac/Linux users: consider using the shell version instead.
#  - Enough disk space for the entire dataset. Full download takes
#    approximately 2 TB of space, 4TB if you also download the
#    pre-dedistorted images. Uncompressed, the dataset requires
#    approximately XXTB, YYTB including lossless dedistorted imagery.
#
# By using or modifying this script to download MSU-4S,
# you hereby agree to the terms listed below.
$license="The Michigan State University Four Seasons (MSU-4S) Dataset
(c)2024 Michigan State University.

The Michigan State University Four Seasons (MSU-4S) Dataset is provided as-is,
with no warranty or guarantee of fitness for any use, under the Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International license. 

This license requires that reusers give credit to the creator. It allows reusers
to distribute, remix, adapt, and build upon the material in any medium or format,
for noncommercial purposes only. If others modify or adapt the material, they
must license the modified material under identical terms."

# Download priority. Can be foreground, high, normal, or low.
# All options other than foreground defer bandwidth to other foreground processes.
$priority='foreground'
# Valid segments: 2022_rain 2022_spring 2022_spring_snow 2023_early_fall 2023_fall_sunset 2023_fall_sunset_2 2023_late_summer 2023_neighborhood_fall 2023_snow
$segments=@("2022_rain", "2022_spring", "2022_spring_snow", "2023_early_fall", "2023_fall_sunset", "2023_fall_sunset_2", "2023_late_summer", "2023_neighborhood_fall", "2023_snow")
# To choose data channels, add/remove them from the following line
# Valid sources: misc oust top_left top_left_dd top_left_dd_jpg top_mid top_mid_dd top_mid_dd_jpg top_right top_right_dd top_right_dd_jpg
$data_sources=@("misc", "top_left", "top_mid", "top_right", "oust", "velo")
# Directory to save to. Defaults to current directory if left blank.
$output_dir="S:\kd3\4s"

$license_prompt = $license + "

Do you accept the terms of this license?"

$title = 'MSU-4S License Agreement'
$choices = '&Yes', '&No'

$decision = $Host.UI.PromptForChoice($title, $license_prompt, $choices, 1)
if ($decision -eq 1) {
    Write-Host 'License not accepted. Halting download.'
    Exit
}
Write-Host 'License accepted. Download commencing.'

$base_url='http://mirrors.egr.msu.edu/canvas/msu4s'

try {
    New-Item -ItemType Directory -Force -Path ${output_dir} | Out-Null
    Resolve-Path -Path "$output_dir" -OutVariable output_path -ErrorVariable success | Out-Null
}
catch {
    Write-Host "Supplied path is invalid. Using current directory."
    try {
        Resolve-Path -Path "." -OutVariable output_path | Out-Null
    }
    catch {
        Write-Host "Unable to resolve any path to download dataset. Halting."
        Exit 1
    }
}
foreach($seg in $segments) {
    $file_sources=[System.Collections.ArrayList]@()
    $file_dests=[System.Collections.ArrayList]@()
    foreach($src in $data_sources) {
        New-Item -ItemType Directory -Force -Path ${output_path}\${seg} | Out-Null
        $url="${base_url}/${seg}/${src}.tar.gz"
        $save_path="${output_path}\${seg}\${src}.tar.gz"
        $file_sources.Add("$url") | Out-Null
        $file_dests.Add("$save_path") | Out-Null
    }
    Start-BitsTransfer -Priority ${priority} -Source ${file_sources} -Destination ${file_dests}
    Get-BitsTransfer | Complete-BitsTransfer
}

# Start-BitsTransfer -Priority ${priority} -Source `"${file_sources}`" -Destination `"${file_dests}`"
